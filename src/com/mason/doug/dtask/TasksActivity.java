/**
 * Homework011116665Activity.java - main activity for our Advanced Tasks Application for Homework 11 for Android Application Development
 * CP310
 * Utilizes ActionBarSherlock in order to allow for the Android ActionBar to be utilized within older API(8+) instead of 11+ which the Fragment API would 
 * normally dictate.
 * 
 * Icon for Application comes from the following site:
 * http://openiconlibrary.sourceforge.net/gallery2/?./Icons/actions/appointment-new-4.png
 * 
 * Author: Doug Mason
 */

package com.mason.doug.dtask;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class TasksActivity extends SherlockFragmentActivity implements TaskListFragment.onItemSelectedListener,ActionBar.OnNavigationListener,LoaderManager.LoaderCallbacks<Cursor> {
	private static final String PREFS = "prefs";
	private static final String ACCOUNT = "account";
	private static final String TOKEN = "token";
	private static final int DIALOG_NEW = 25;
	public static final String LIST_TAG = "list";
	public static final String DETAILS_TAG = "details";
	private static final int TASKS_LOADER = 1;
	private SimpleCursorAdapter adapter;
	public SimpleCursorAdapter getAdapter() {
		return adapter;
	}


	public void setAdapter(SimpleCursorAdapter adapter) {
		this.adapter = adapter;
	}


	private TaskListFragment listFragment;
	private static final int DIALOG_DELETE = 66;
	private static final int DIALOG_ACCOUNTS = 100;
	private static final String AUTH_TOKEN_TYPE= "Manage your tasks";
	private Account syncAccount;
	private AccountManager manager;
	EditText text;
	private Account[] accounts;
//	com.google.api.services.tasks.Tasks service;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		String[] from = {TasksDBHelper.NOTE};
		int[] to = {android.R.id.text1};
//		getLoaderManager().initLoader(TASKS_LOADER, null, this);
		getSupportLoaderManager().initLoader(TASKS_LOADER, savedInstanceState, this);
		adapter = new SimpleCursorAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,null,from,to,CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        Context context = getSupportActionBar().getThemedContext();
        ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(context, R.array.NoteItems, R.layout.sherlock_spinner_item);
        list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
        listFragment = new TaskListFragment();
        
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(listFragment, LIST_TAG);
        transaction.add(R.id.fragment_container, listFragment);
        transaction.commit();
       
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setListNavigationCallbacks(list, this);
        manager = AccountManager.get(this);
        text = new EditText(this);
        this.accounts = manager.getAccountsByType("com.google");
    }
    
    
    /**
     * Standard inflation of options menu/actionbar
     */
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	getSupportMenuInflater().inflate(R.menu.main, menu);
	return true;
	}
    
/**
 * Handling options events. In order to do this I use dialogs, although it looks like I will need to at some point soon look into utilizing fragments
 * for this functionality according to Google. Will utilize class level constant integers in order to determine which Dialog to return and display on the
 * screen.
 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		TaskDetailsFragment frag = (TaskDetailsFragment) getSupportFragmentManager().findFragmentByTag(DETAILS_TAG);
		// TODO Auto-generated method stub
		switch (item.getItemId())
		{
		case R.id.authenticateUser:
			showDialog(DIALOG_ACCOUNTS);
			return true;
		case R.id.newItem:
			showDialog(DIALOG_NEW);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, frag);
			transaction.addToBackStack(null);
			transaction.commit();
			frag.clear();
			frag.setNote(-1);
			return true;
		case R.id.saveNote:
			ContentValues cv = new ContentValues();
			cv.put(TasksDBHelper.NOTE, frag.getDetailText().getText().toString());
			getContentResolver().update(TasksProvider.CONTENT_URI, cv, TasksDBHelper.ID+"="+Integer.toString(frag.getNoteId()), null);
			return true;
		case R.id.deleteItem:
			getContentResolver().delete(TasksProvider.CONTENT_URI, TasksDBHelper.ID+"="+Integer.toString(frag.getNoteId()), null);
			frag.clear();
			return true;
		}
		return false;
	}


/**
 * Builds the dialog for how to handle specific tasks 
 */
	protected Dialog onCreateDialog(int id)
    {
    	switch(id)
    	{
    	case DIALOG_ACCOUNTS:
    		AlertDialog.Builder build = new AlertDialog.Builder(this);
    		build.setTitle("Select your Google Account");
    		final int size = accounts.length;
    		String[] names = new String[size];
    		for(int i = 0; i < size; i++)
    		{
    			names[i] = accounts[i].name;
    		}
    		build.setItems(names, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					gotAccount(accounts[which]);
				}
			});
    		return build.create();
    	case DIALOG_NEW:
    		AlertDialog.Builder newTask = new AlertDialog.Builder(this);
    		newTask.setTitle("Create your task");
    		newTask.setView(text);
    		newTask.setPositiveButton("OK",new DialogInterface.OnClickListener(){
    		public void onClick(DialogInterface d,int what){
    			String str =  TasksActivity.this.text.getText().toString();
    			ContentValues cv = new ContentValues();
    			cv.put(TasksDBHelper.NOTE, str);
    			getContentResolver().insert(TasksProvider.CONTENT_URI, cv);
    		}
    		});
    		return newTask.create();
    	case DIALOG_DELETE:
    		TaskDetailsFragment frag = (TaskDetailsFragment) getSupportFragmentManager().findFragmentByTag(DETAILS_TAG);
    			Log.v("Deleting",Integer.toString(frag.getNoteId()));
    			getContentResolver().delete(TasksProvider.CONTENT_URI, TasksDBHelper.ID+"="+Integer.toString(frag.getNoteId()), null);
    		break;
    		
    		default:
    			break;
    	}
    	return null;
    }
	
	//Since I use AlertDialog.Builder and an edittext instead of going through and creating dialogs and layouts for my dialog boxes
	//I had to create a class-specific instance of EditText to allow for my anonymous inline onClickListener for my dialog box to get the text, and for the
	//Text to be cleared out whenever we created a new note. 
	protected void onPrepareDialog(int id, Dialog d)
	{
		switch(id){
		case DIALOG_NEW:
			text.setText("");
			break;
		}
	}
	
	/**
	 * 
	 * @param a - an account that we will have received the authentication token for. This will allow for us to Synchronize our tasks to the 
	 * Google Tasks API.(I Hope)
	 */
    protected void gotAccount(Account a)
    {
    	Log.v("Account obtained!",a.name);
    	this.syncAccount = a;
    	manager.getAuthToken(syncAccount, AUTH_TOKEN_TYPE, null, this, new AccountManagerCallback<Bundle>()
    			{
    		public void run(AccountManagerFuture<Bundle> future)
    		{
    			try{
    				String token = future.getResult().getString(AccountManager.KEY_AUTHTOKEN);
    				SharedPreferences prefs = getSharedPreferences(PREFS, MODE_PRIVATE);
    				SharedPreferences.Editor edit = prefs.edit();
    				edit.putString(ACCOUNT,syncAccount.name );
    				edit.putString(TOKEN, token);
    				edit.commit();
    			}
    			catch(Exception e)
    			{
    				e.printStackTrace();
    			}
    		}
    		
    			},null);
    	
    }
    
/**
 * This will allow for us to view the details of our tasks, we had to set this up within the list fragment to allow for the application to listen to when a
 * list item is clicked, and then when it is I set it up to be the content of our detail view. This also allows for us to select and delete tasks.
 */
	@Override
	public void onItemSelected(int pos) {
		TaskListFragment list = (TaskListFragment)getSupportFragmentManager().findFragmentByTag(LIST_TAG);
//		TaskDetailsFragment detail = (TaskDetailsFragment)getSupportFragmentManager().findFragmentByTag(DETAILS_TAG);
		TaskDetailsFragment fragment = new TaskDetailsFragment();
		fragment.setNote((int)list.getListAdapter().getItemId(pos));
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(fragment, DETAILS_TAG);
		transaction.replace(R.id.fragment_container, fragment);
		transaction.addToBackStack(null);
		transaction.commit();

		Log.v("Position",Integer.toString(fragment.getNoteId()));
	}


@Override
public boolean onNavigationItemSelected(int itemPosition, long itemId) {
	switch(itemPosition){
	case 0:
		Toast.makeText(getApplicationContext(), "Selected first option", Toast.LENGTH_SHORT).show();
		break;
	case 1:
		Toast.makeText(getApplicationContext(), "Selected second option", Toast.LENGTH_LONG).show();
		break;
	case 2:
		break;
		
	}
	return false;
}


@Override
public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
	String[] projection = TasksDBHelper.PROJECTION;
	CursorLoader loader = new CursorLoader(getBaseContext(),TasksProvider.CONTENT_URI,projection,null,null,null);
	return loader;
}


@Override
public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
	// TODO Auto-generated method stub
	adapter.swapCursor(arg1);
}


@Override
public void onLoaderReset(Loader<Cursor> arg0) {
	// TODO Auto-generated method stub
	adapter.swapCursor(null);
}
    
}