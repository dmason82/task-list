/**
 * TaskListFragment.java - Fragment to display all of our tasks in a list.
 */

package com.mason.doug.dtask;

//ActionbarSherlock class path
import com.actionbarsherlock.app.SherlockListFragment;

//Android native classes
import android.app.Activity;
import android.os.Bundle;

//Android Compatibility library
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;


public class TaskListFragment extends SherlockListFragment{
	private onItemSelectedListener listen;
	private TasksActivity parent;
	private SimpleCursorAdapter adapter;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		listen = (onItemSelectedListener) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		parent =(TasksActivity)getActivity();
		adapter = parent.getAdapter();
		setListAdapter(adapter);
	}


	
	@Override
	public void onListItemClick(ListView l, View v,int pos, long id)
	{
		listen.onItemSelected(pos);
	}
	
	
	public interface onItemSelectedListener{
		public void onItemSelected(final int pos);
	}
	
}
