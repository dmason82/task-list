package com.mason.doug.dtask;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.CheckBox;

import com.actionbarsherlock.app.SherlockFragment;
import com.mason.doug.dtask.R;

public class TaskDetailsFragment extends SherlockFragment implements OnClickListener{
	private EditText detailText,descriptionText;
	private CheckBox completedBox;
	private Cursor c;
	private TasksActivity parent;
	private SimpleCursorAdapter adapter;
	//ID of the note
	private int noteId = -1;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		parent =(TasksActivity)getActivity();
		adapter = parent.getAdapter();
		c = adapter.getCursor();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v  = inflater.inflate(R.layout.details, container,false);
		this.detailText = (EditText) v.findViewById(R.id.detailText);
		Button b  = (Button)v.findViewById(R.id.saveNoteButton);
		b.setOnClickListener(this);
		this.completedBox = (CheckBox)v.findViewById(R.id.completedCheckBox);
		this.descriptionText = (EditText)v.findViewById(R.id.descriptionText);
		c = adapter.getCursor();
		this.updateDisplay();
		return v;
	}
	
	public void setNote(int i){
		this.noteId = i;
//		updateDisplay();
	}
	@Override
	public void onClick(View v) {
		Log.v("TEST","onClick() called");
		switch(v.getId()){
		case R.id.saveNoteButton:
			ContentValues cv = new ContentValues();
			cv.put(TasksDBHelper.NOTE,this.detailText.getText().toString());
			cv.put(TasksDBHelper.DETAILS, this.descriptionText.getText().toString());
			cv.put(TasksDBHelper.COMPLETED, completedBox.isChecked()?1:0);
			if(this.noteId !=-1){
			Log.v("Saving note",Integer.toString(this.noteId));
			getActivity().getContentResolver().update(TasksProvider.CONTENT_URI, cv, TasksDBHelper.ID+"="+this.noteId, null);
			}
			else{
				getActivity().getContentResolver().insert(TasksProvider.CONTENT_URI, cv);
				
			}
		}
	}
	
	public int getNoteId()
	{
		return this.noteId;
	}
	/**
	 * 
	 * Getter 
	 */
	public EditText getDetailText()
	{
		return this.detailText;
	}
	public void clear()
	{
		this.noteId = -1;
		this.detailText.setText("");
		this.descriptionText.setText("");
		this.completedBox.setText("");
	}

	private void updateDisplay(){
		Log.v("WHAT_IS_NULL", this.detailText.getText().toString());
		c = adapter.getCursor();
		this.detailText.setText(c.getString(c.getColumnIndex(TasksDBHelper.NOTE)));
		this.descriptionText.setText(c.getString(c.getColumnIndex(TasksDBHelper.DETAILS)));
		this.completedBox.setChecked(c.getInt(c.getColumnIndex(TasksDBHelper.COMPLETED))==1?true:false);
	}
}
