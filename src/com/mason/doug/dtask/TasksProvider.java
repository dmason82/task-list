/**
 * TasksProvider.java - A content provider for the Homework 11 Tasks Application.
 * I figured that if we are going to have to run a synchronization task with a 3rd party and have two different fragments hitting the database, that I 
 * would rather make sure that Android takes care of the data operations as much as possible, which makes content providers very, very handy.
 */
package com.mason.doug.dtask;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * @author Doug Mason
 *
 */
public class TasksProvider extends ContentProvider {
	private SQLiteDatabase tasksDB;
	public static final int TASKS = 10;
	public static final int TASKS_ID= 20;


	private static final String AUTHORITY = "com.mason.doug.dtask.TasksProvider";
	private static final String BASE_PATH = "tasks";
	public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/"+BASE_PATH);
	
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE+"/tasks";
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE+"/task";
	public static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
	static {
	MATCHER.addURI(AUTHORITY, BASE_PATH, TASKS);
	MATCHER.addURI(AUTHORITY, BASE_PATH + "/#", TASKS_ID);
}
	/**
	 * 
	 */
	public TasksProvider() {
		// TODO Auto-generated constructor stub
		super();
	}
/**
 * Removes item from the Database, checks for whether or not it is a task, and then if so, it will run an SQL query against our database and remove the object from 
 * existence
 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = MATCHER.match(uri);
		Log.v("URI Type: ",Integer.toString(uriType));
		int deleted = 0;
		Log.v("URI", uri.toString());
//		String id = uri.getLastPathSegment();
		switch(uriType)
		{
		case TASKS:
			deleted = tasksDB.delete(TasksDBHelper.TABLE, selection, selectionArgs);
			break;
		case TASKS_ID:
			break;
			default:
				throw new IllegalArgumentException("Unknown URI "+uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return deleted;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}
/**
 * Inserts a new object into our provider
 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = MATCHER.match(uri);
		long id = 0;
		Log.v("URI Type: ",Integer.toString(uriType));
		/**
		 * What specifically are we dealing with? If it isn't a task, then we throw an exception because this is a one-typed content provider.
		 */
		switch(uriType){
		case TASKS:
			id = tasksDB.insert(TasksDBHelper.TABLE, null, values);
			break;
			default:
				//We don't know what todo with the URI specified
				throw new IllegalArgumentException("Unknown URI" +uri);
		}
		getContext().getContentResolver().notifyChange(uri,null);
		//returns the URI for the newly created object in our Tasks DB
		Log.v("Row inserted into DB: ", Long.toString(id));
		return Uri.parse(BASE_PATH+"/"+id);
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		TasksDBHelper helper = new TasksDBHelper(getContext());
		tasksDB = helper.getWritableDatabase();
		return false;
	}
	/**
	 * Provides a cursor for gathering data via our Content provider
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		int uriType = MATCHER.match(uri);
		switch (uriType)
		{
		case TASKS:
			Cursor c = tasksDB.query(TasksDBHelper.TABLE, projection, selection, selectionArgs, null, null, null);
			c.setNotificationUri(getContext().getContentResolver(), uri);
			return c;
			default:
				throw new IllegalArgumentException("Unknown URI Type: "+uri);
			
		}
	}

	/**
	 * updates our SQL Database based on the URI to what the values should be for our current item.
	 * 
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		synchronized(tasksDB){
			int uriType = MATCHER.match(uri);
			int updated = 0;
			Log.v("Updating something",selection);
			switch(uriType)
			{
			case TASKS:
				updated = tasksDB.update(TasksDBHelper.TABLE, values, selection, selectionArgs);
				break;
				default:
					throw new IllegalArgumentException("Unknown URI "+uri);
			}
			getContext().getContentResolver().notifyChange(uri, null);
			return updated;
			}
		}
}
