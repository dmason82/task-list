/**
 * TasksDBHelper.java - Database helper class that contains the basic create/update tasks for our Database for the tasks application.
 * Also contains constants to allow us to utilize the DB within other applications, including the content provider and associated fragments.
 */
package com.mason.doug.dtask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class TasksDBHelper extends SQLiteOpenHelper {
	public static final String NOTE = "note";
	public static final String TABLE = "tasks";
	public static final String DETAILS = "details";
	public static final String COMPLETED = "completed";
	public static final String[] PROJECTION = {TasksDBHelper.ID,TasksDBHelper.NOTE,TasksDBHelper.DETAILS,TasksDBHelper.COMPLETED};
	public static final int DB_VER = 2;
	public static final String ID = "_id";
	private static final String CREATE_TASKS_TABLE  = 
            "CREATE TABLE " + 
            TasksDBHelper.TABLE + " (" + 
            TasksDBHelper.ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " 
            +TasksDBHelper.NOTE + " TEXT," +TasksDBHelper.DETAILS+" TEXT, "+TasksDBHelper.COMPLETED+" INTEGER "+");"  ;
	
	public TasksDBHelper(Context c){
		super(c,TABLE,null,DB_VER);
	}
	/**
	 * runs to create our tasks table in the tasks database.
	 */
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		arg0.execSQL(CREATE_TASKS_TABLE);
	}
	/**
	 * Our upgrade procedure is pretty simplistic at the moment,
	 * Deletes all data in the table and then recreates the table using the schema we specified as a 
	 * class variable.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS "+TABLE);
		db.execSQL(CREATE_TASKS_TABLE);
	}

}
